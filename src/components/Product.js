import React from "react";

const Product = ({ details }) => {
  const { title, category, price, description } = details;
  return (
    <div className="product col-12 col-md-3 col-lg-4">
      <div className="product-img">
        <img className="img-responsive" src="https://img.icons8.com/nolan/96/add-shopping-cart.png" alt="product-icon" />
      </div>
      <h1>{title}</h1>
      <h5>{price}$</h5>
      <p>
        Categrorie: <strong>{category}</strong>
      </p>
      <p className="product-description">{description}</p>
    </div>
  );
};

export default Product;
