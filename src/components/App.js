import React from "react";
import products from "../store/products.js";
import ProductsList from "./ProductsList.js";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prods: [],
      categories: [],
    };
  }
  componentDidMount = () => {
    this.setState({ prods: products.list });
    var CategoriesValues = [];
    Object.keys(products.list).map((key) => {
      const exist = CategoriesValues.includes(products.list[key].category);
      if (exist == false) {
        CategoriesValues.push(products.list[key].category);
      }
    });
    this.setState({ categories: CategoriesValues });
  };
  render() {
    return (
      <div className="container-fluid">
        <ProductsList prods={this.state.prods} categories={this.state.categories} />
      </div>
    );
  }
}

export default App;
