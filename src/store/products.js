var products = {
    list: [
        {
            "title": "Desk Chair",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ipsum mi, sodales ac justo a, vulputate aliquet massa.",
            "category": "furniture",
            "price": 100
        },
        {
            "title": "Office Cabinet",
            "description": "Mauris lobortis aliquam scelerisque. Cras non varius nisi. Donec eleifend, est id ornare pellentesque.",
            "category": "furniture",
            "price": 50
        },
        {
            "title": "Washer",
            "description": "Quisque nec vehicula felis. Nam id massa metus. Duis pharetra felis in leo lacinia sollicitudin.",
            "category": "appliance",
            "price": 2000
        },
        {
            "title": "Refrigerator",
            "description": "Sed lorem augue, pulvinar et rhoncus a, molestie ac mauris. Aliquam erat volutpat. Vivamus hendrerit rutrum sapien, at vestibulum erat fringilla et.",
            "category": "appliance",
            "price": 3000
        },
        {
            "title": "Laptop",
            "description": "Maecenas quam risus, tincidunt semper volutpat eget, volutpat sed magna.",
            "category": "high-tech",
            "price": 1500
        },
        {
            "title": "Headphone",
            "description": "In scelerisque arcu vitae hendrerit pretium. Mauris justo est, mollis hendrerit efficitur nec, tristique eu est.",
            "category": "high-tech",
            "price": 150
        }
    ]
};
export default products;
