import React, { useState, useEffect } from "react";
import Products from "./Products.js";
import Select from "react-select";

const ProductsList = ({ prods, categories }) => {
  const [filtre, setState] = useState({
    title: "",
    category: "",
    price: 0,
  });
  const [data, setData] = useState([]);

  const getOptions = () => {
    let options = [];
    options = categories.map((key) => {
      return { value: key, label: key };
    });
    return options;
  };

  const defaultValue = () => {
    return { value: categories[0], label: categories[0] };
  };

  const selectCategory = (selectedValue) => {
    setState({
      ...filtre,
      category: selectedValue.value,
    });
  };

  const selectedValue = () => {
    const cat = filtre.category;
    if (filtre.category.length > 0) {
      return { value: cat, label: cat };
    }
  };

  const HandleInput = (e) => {
    setState({
      ...filtre,
      [e.target.name]: e.target.value,
    });
  };

  let filteredProducts = data;

  const getFilteredProds = () => {
    setData(prods);
    if (filtre.category.length > 0) {
      const Categoriyfilter = prods.filter(
        (key) => key.category == filtre.category
      );
      setData(Categoriyfilter);
    }
    if (filtre.title.length > 0) {
      const Titlefilter = prods.filter((key) =>
        key.title.includes(filtre.title)
      );
      setData(Titlefilter);
    }
    if (filtre.price.length > 0) {
      const Titlefilter = prods.filter((key) =>
        parseInt(key.price) <= parseInt(filtre.price)
      );
      setData(Titlefilter);
    }
  };
  useEffect(() => {
    getFilteredProds();
  });

  return (
    <div className="div-container row">
      <div className="filter col-md-3">
        <form className="form-group mt-5">
          <input
            className="form-control mt-7 mb-3"
            type="text"
            name="title"
            placeholder="Chercher par titre de produits"
            onChange={HandleInput}
          />
          <Select
            className="react-select-container mb-3"
            classNamePrefix="react-select"
            defaultValue={defaultValue}
            placeholder="Catégorie"
            value={selectedValue()}
            isSearchable={false}
            options={getOptions()}
            onChange={selectCategory}
          />
          <input
            className="form-control mt-7 mb-3"
            type="text"
            name="price"
            placeholder="Filtrer par prix max"
            onChange={HandleInput}
          />
        </form>
      </div>
      <div className="list col-md-9 row">
        <Products prods={filteredProducts} />;
      </div>
    </div>
  );
};

export default ProductsList;
