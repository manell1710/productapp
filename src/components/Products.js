import React, { useState } from "react";
import Product from "./Product.js";
import icon from "../assets/images/sort-arrows.png";

const Products = ({ prods }) => {
  const [sortDirection, setSortDirection] = useState(true);
  const sortedProducts = React.useMemo(
    () =>
      prods.sort((a, b) => {
        if (a.title < b.title) {
          return sortDirection === true ? -1 : 1;
        } else return sortDirection === false ? -1 : 1;
      }),
    [prods, sortDirection]
  );
  return (
    <div className="list col-md-9 ">
      <div className="sort-icon row">
        <span>Trier :</span>
        <img src={icon} alt="sort-icon" onClick={() => setSortDirection(!sortDirection)} />
      </div>
      <div className="row">
        {Object.keys(sortedProducts).map((key) => {
          return <Product key={key} details={prods[key]} />;
        })}
      </div>
    </div>
  );
};

export default Products;
